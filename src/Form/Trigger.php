<?php

namespace Drupal\config_auto_export\Form;

use Drupal\config_auto_export\Service;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trigger the webhook to process exported config.
 */
class Trigger extends ConfirmFormBase {

  /**
   * The Config Auto Export service.
   *
   * @var \Drupal\config_auto_export\Service
   */
  protected Service $service;

  /**
   * Trigger constructor.
   *
   * @param \Drupal\config_auto_export\Service $service
   *   The service for the Config Auto Export.
   */
  public function __construct(Service $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Trigger {
    return new static(
      $container->get('config_auto_export.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Trigger webhook for exported config changes?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t('If you confirm, this will trigger the webhook to process the exported config changes since this ran last time.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Trigger now');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('config_auto_export.trigger');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config_auto_export_trigger';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    // Remove the Cancel button as it doesn't make much sense here.
    unset($form["actions"]["cancel"]);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($this->service->triggerExport(TRUE)) {
      $this->messenger()->addStatus($this->t('Successfully triggered webhook.'));
    }
    else {
      $this->messenger()->addError($this->t('Webhook can not be triggered!'));
    }
  }

}
