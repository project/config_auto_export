<?php

namespace Drupal\config_auto_export;

use Drupal\Component\Datetime\Time;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\config\StorageReplaceDataWrapper;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\TypedConfigManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Session\AccountInterface;

/**
 * The Service for the Config Auto Export.
 */
class Service {

  public const STATE_KEY_DUE_TIMESTAMP = 'config_auto_export.due_next.timestamp';

  public const STATE_KEY_PAUSED = 'config_auto_export.paused';

  /**
   * An immutable configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The file handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Returns the state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $time;

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected ClientFactory $client;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The cache for the storage.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected CachedStorage $storage;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected ConfigManager $configManager;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManager
   */
  protected TypedConfigManager $configTyped;

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected ModuleInstallerInterface $moduleInstaller;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  protected ThemeHandler $themeHandler;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected TranslationManager $stringTranslation;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $extensionListModule;

  /**
   * The extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * Service constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger instance.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage service.
   * @param \Drupal\Component\Datetime\Time $time
   *   The time service.
   * @param \Drupal\Core\Http\ClientFactory $client
   *   The client factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module installer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\CachedStorage $storage
   *   The cache for the storage.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigManager $config_manager
   *   The configuration manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   * @param \Drupal\Core\Config\TypedConfigManager $config_typed
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $module_installer
   *   The module installer.
   * @param \Drupal\Core\Extension\ThemeHandler $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\StringTranslation\TranslationManager $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver service.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    LoggerChannelFactoryInterface $logger_channel_factory,
    StateInterface $state,
    Time $time,
    ClientFactory $client,
    AccountInterface $account,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    CachedStorage $storage,
    ContainerAwareEventDispatcher $event_dispatcher,
    ConfigManager $config_manager,
    LockBackendInterface $lock,
    TypedConfigManager $config_typed,
    ModuleInstallerInterface $module_installer,
    ThemeHandler $theme_handler,
    TranslationManager $string_translation,
    ModuleExtensionList $extension_list_module,
    ExtensionPathResolver $extension_path_resolver
  ) {
    $this->config = $config_factory->get('config_auto_export.settings');
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->logger = $logger_channel_factory->get('config');
    $this->state = $state;
    $this->time = $time;
    $this->client = $client;
    $this->account = $account;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->storage = $storage;
    $this->eventDispatcher = $event_dispatcher;
    $this->configManager = $config_manager;
    $this->lock = $lock;
    $this->configTyped = $config_typed;
    $this->moduleInstaller = $module_installer;
    $this->themeHandler = $theme_handler;
    $this->stringTranslation = $string_translation;
    $this->extensionListModule = $extension_list_module;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * Pause config auto export.
   */
  public function pause(): void {
    $this->state->set(self::STATE_KEY_PAUSED, TRUE);
  }

  /**
   * Resume config auto export.
   */
  public function resume(): void {
    $this->state->set(self::STATE_KEY_PAUSED, FALSE);
  }

  /**
   * Is the process currently paused?
   *
   * @return bool
   *   Returns if the status is paused or not.
   */
  public function isPaused(): bool {
    return $this->state->get(self::STATE_KEY_PAUSED, FALSE);
  }

  /**
   * Trigger the webhook.
   *
   * @param bool $force
   *   Force trigger even if service is paused.
   *
   * @return bool
   *   Returns if the auto export fails or not.
   */
  public function triggerExport(bool $force = FALSE): bool {
    if (!$force && $this->isPaused()) {
      return FALSE;
    }
    if (!$this->config->get('enabled')) {
      return FALSE;
    }
    $webhook = $this->config->get('webhook');
    if (empty($webhook)) {
      return FALSE;
    }
    if (!$force && !$this->config->get('webhook_autorun_enabled')) {
      return FALSE;
    }
    $exportPath = $this->fileSystem->realpath($this->config->get('directory'));
    $configPath = $this->fileSystem->realpath(Settings::get('config_sync_directory'));
    $currentUser = $this->account->getDisplayName();
    $configSplitPaths = [];
    if ($this->moduleHandler->moduleExists('config_split')) {
      try {
        /** @var \Drupal\config_split\Entity\ConfigSplitEntityInterface $split */
        foreach ($this->entityTypeManager->getStorage('config_split')->loadMultiple() as $split) {
          if ($split->status()) {
            $configSplitPaths[$split->id()] = $split->get('folder');
          }
        }
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        // We ignore this as we just don't add split config.
      }
    }
    $data = [
      'form_params' => Yaml::decode(str_replace(
        [
          '[current user]',
          '[export directory]',
          '[config directory]',
          '[config split directories]',
        ],
        [
          $currentUser,
          $exportPath,
          $configPath,
          json_encode($configSplitPaths),
        ],
        $this->config->get('webhook_params'))),
    ];

    try {
      $client = $this->client->fromOptions(['base_uri' => $webhook]);
      $client->request('post', '', $data);
    }
    catch (GuzzleException $e) {
      $this->logger->critical('Trigger for config auto export failed: {msg}', ['msg' => $e->getMessage()]);
      return FALSE;
    }
    $this->state->delete(self::STATE_KEY_DUE_TIMESTAMP);
    return TRUE;
  }

  /**
   * Check if a trigger due date exists and is due, then call the trigger.
   */
  public function checkDueDate(): void {
    if (($dueTime = $this->state->get(self::STATE_KEY_DUE_TIMESTAMP)) && $dueTime <= $this->time->getRequestTime()) {
      $this->triggerExport();
    }
  }

  /**
   * Reload a config file from a module's install or optional directory.
   *
   * @param string $module
   *   The module to be used.
   * @param string $section
   *   The section of the user.
   * @param string[] $configIds
   *   Array of Ids.
   */
  public function reloadConfig(string $module, string $section, array $configIds): void {
    if (!$this->moduleHandler->moduleExists($module)) {
      $this->logger->alert('Reload config was called for %module, which is not enabled.', [
        '%module' => $module,
      ]);
      return;
    }
    if (!in_array($section, ['install', 'optional', 'overwrite'])) {
      $this->logger->alert('Reload config was called for %module with section %section, which is not allowed.', [
        '%module' => $module,
        '%section' => $section,
      ]);
      return;
    }

    foreach ($configIds as $configId) {
      $filename = $this->extensionPathResolver->getPath('module', $module) . "/config/$section/$configId.yml";
      if (!file_exists($filename)) {
        $this->logger->alert('Reload config was called for %module with section %section and config ID %configid, which does not exist.', [
          '%module' => $module,
          '%section' => $section,
          '%configid' => $configId,
        ]);
        continue;
      }
      $data = Yaml::decode(file_get_contents($filename));
      if ($config = $this->configFactory->getEditable($configId)) {
        if ($uuid = $config->get('uuid')) {
          $data['uuid'] = $uuid;
        }
        $config->setData($data)->save();
      }
      else {
        $this->createConfig($configId, $data);
      }
    }
  }

  /**
   * Creates a new configuration.
   *
   * @param string $configId
   *   The Id of the configuration.
   * @param array $data
   *   The array that contains all the configuration.
   */
  public function createConfig(string $configId, array $data): void {
    $sourceStorage = new StorageReplaceDataWrapper($this->storage);
    $sourceStorage->delete($configId);
    $sourceStorage->write($configId, $data);
    $storageComparer = new StorageComparer($sourceStorage, $this->storage);
    $configImporter = new ConfigImporter(
      $storageComparer,
      $this->eventDispatcher,
      $this->configManager,
      $this->lock,
      $this->configTyped,
      $this->moduleHandler,
      $this->moduleInstaller,
      $this->themeHandler,
      $this->stringTranslation,
      $this->extensionListModule,
    );
    if ($configImporter->alreadyImporting()) {
      $this->logger->alert('Import already running.');
      return;
    }
    if (!$configImporter->validate()) {
      $this->logger->alert('To be imported config not valid.');
      return;
    }
    $sync_steps = $configImporter->initialize();
    foreach ($sync_steps as $step) {
      $context = [];
      do {
        $configImporter->doSyncStep($step, $context);
      } while ($context['finished'] < 1);
    }
  }

}
