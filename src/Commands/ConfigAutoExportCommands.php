<?php

namespace Drupal\config_auto_export\Commands;

use Drupal\config_auto_export\Service;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Diff\DiffFormatter;
use Drupal\Core\Render\RendererInterface;
use Drush\Commands\DrushCommands;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A Drush commandfile.
 */
class ConfigAutoExportCommands extends DrushCommands {
  use StringTranslationTrait;

  /**
   * The interface for the ConfigManager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected ConfigManagerInterface $configManager;

  /**
   * The formatter for the differences in the configs.
   *
   * @var \Drupal\Core\Diff\DiffFormatter
   */
  protected DiffFormatter $diffFormatter;

  /**
   * The cache for the storage.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected CachedStorage $cachedStorage;

  /**
   * The file storage location.
   *
   * @var \Drupal\Core\Config\FileStorage
   */
  protected FileStorage $fileStorage;

  /**
   * The Config Auto Export service.
   *
   * @var \Drupal\config_auto_export\Service
   */
  protected Service $service;

  /**
   * The interface for the renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * ConfigAutoExportCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $configManager
   *   The interface for the configuration manager.
   * @param \Drupal\Core\Diff\DiffFormatter $diffFormatter
   *   The formatter for the differences in the configs.
   * @param \Drupal\Core\Config\CachedStorage $cachedStorage
   *   The cache for the storage.
   * @param \Drupal\Core\Config\FileStorage $fileStorage
   *   The file location for store.
   * @param \Drupal\config_auto_export\Service $service
   *   The service for the Config Auto Export.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The interface for the renderer.
   */
  public function __construct(ConfigManagerInterface $configManager, DiffFormatter $diffFormatter, CachedStorage $cachedStorage, FileStorage $fileStorage, Service $service, RendererInterface $renderer) {
    parent::__construct();
    $this->configManager = $configManager;
    $this->diffFormatter = $diffFormatter;
    $this->cachedStorage = $cachedStorage;
    $this->fileStorage = $fileStorage;
    $this->service = $service;
    $this->renderer = $renderer;
  }

  /**
   * Trigger config auto export.
   *
   * @command cae:trigger
   */
  public function trigger(): void {
    if ($this->service->triggerExport()) {
      $this->io()->success('Successfully triggered config export.');
    }
    else {
      $this->io()->error('Config export can not be triggered!');
    }
  }

  /**
   * Get status of config auto export.
   *
   * @command cae:status
   */
  public function status(): void {
    if ($this->service->isPaused()) {
      $this->io()->warning('Config auto export is paused.');
    }
    else {
      $this->io()->success('Config auto export is running.');
    }
  }

  /**
   * Pause config auto export.
   *
   * @command cae:pause
   */
  public function pause(): void {
    $this->service->pause();
    $this->io()->success('Config auto export paused.');
  }

  /**
   * Resume config auto export.
   *
   * @command cae:resume
   */
  public function resume(): void {
    $this->service->resume();
    $this->io()->success('Config auto export resumed.');
  }

  /**
   * Re-import config auto export.
   *
   * @command cae:re-import
   */
  public function reImport(): void {
    $diffs = [];
    $this->diffFormatter->show_header = FALSE;
    foreach ($this->fileStorage->listAll() as $name) {
      $diff = $this->configManager->diff($this->cachedStorage, $this->fileStorage, $name);
      if ($diff->isEmpty()) {
        // Nothing changed, delete that item.
        $this->fileStorage->delete($name);
        continue;
      }

      $build = [];
      $build['#title'] = $this->t('View changes of @config_file', ['@config_file' => $name]);
      $build['#attached']['library'][] = 'system/diff';
      $build['diff'] = [
        '#type' => 'table',
        '#attributes' => [
          'class' => ['diff'],
        ],
        '#header' => [
          ['data' => $this->t('Active'), 'colspan' => '2'],
          ['data' => $this->t('Staged'), 'colspan' => '2'],
        ],
        '#rows' => $this->diffFormatter->format($diff),
      ];
      $diffs[$name] = $this->renderer->renderPlain($build);

      // Re-import the cae config data.
      $this->cachedStorage->write($name, $this->fileStorage->read($name));
    }
    if (!empty($diffs)) {
      foreach ($diffs as $name => $html) {
        $this->io()->write('<h2>' . $name . '</h2>' . $html);
      }
      $this->service->triggerExport(TRUE);
    }
  }

}
