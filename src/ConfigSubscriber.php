<?php

namespace Drupal\config_auto_export;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DestructableInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\State\StateInterface;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config subscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface, DestructableInterface {

  /**
   * An immutable configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The cache for the storage.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected CachedStorage $configCache;

  /**
   * The file storage location.
   *
   * @var \Drupal\Core\Config\FileStorage
   */
  protected FileStorage $configStorage;

  /**
   * An array for the configuration of the files.
   *
   * @var array
   */
  protected array $configSplitFiles;

  /**
   * An array for the configuration of the modules.
   *
   * @var array
   */
  protected array $configSplitModules;

  /**
   * A variable to hold the status.
   *
   * @var bool
   */
  protected bool $active = TRUE;

  /**
   * A variable to hold the status of the trigger.
   *
   * @var bool
   */
  protected bool $triggerNeeded = FALSE;

  /**
   * The file handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Returns the state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $time;

  /**
   * The auto export service.
   *
   * @var \Drupal\config_auto_export\Service
   */
  protected Service $service;

  /**
   * Constructs a new Settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   An immutable configuration object.
   * @param \Drupal\Core\Config\CachedStorage $config_cache
   *   The cache for the storage.
   * @param \Drupal\Core\Config\FileStorage $config_storage
   *   The file storage location.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   Returns the state storage service.
   * @param \Drupal\Component\Datetime\Time $time
   *   The time service.
   * @param \Drupal\config_auto_export\Service $service
   *   The auto export service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CachedStorage $config_cache, FileStorage $config_storage, FileSystemInterface $file_system, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, StateInterface $state, Time $time, Service $service) {
    $this->config = $config_factory->get('config_auto_export.settings');
    $this->configCache = $config_cache;
    $this->configStorage = $config_storage;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->time = $time;
    $this->service = $service;
  }

  /**
   * Defines the status to enabled if the file and directory is writable.
   *
   * @return bool
   *   Protected function enabled.
   */
  protected function enabled(): bool {
    static $enabled;

    if (!isset($enabled)) {
      $enabled = FALSE;
      if (!InstallerKernel::installationAttempted() && $this->config->get('enabled')) {
        $uri = $this->config->get('directory');
        if ($this->fileSystem->prepareDirectory($uri, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          if (is_writable($uri) || @chmod($uri, 0777)) {
            $enabled = TRUE;
          }
        }
      }
    }

    return $enabled;
  }

  /**
   * Read all config files from config splits, if available.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function readConfigSplitFiles(): void {
    $this->configSplitFiles = [];
    $this->configSplitModules = [];
    if (!$this->moduleHandler->moduleExists('config_split')) {
      return;
    }
    $extension = '.yml';
    $regex = '/' . str_replace('.', '\.', $extension) . '$/';
    /** @var \Drupal\config_split\Entity\ConfigSplitEntityInterface $split */
    foreach ($this->entityTypeManager->getStorage('config_split')->loadMultiple() as $split) {
      if ($split->status()) {
        $this->configSplitModules += $split->get('module');
        foreach ($this->fileSystem->scanDirectory($split->get('folder'), $regex, ['key' => 'filename']) as $filename => $item) {
          $item->config_split_id = $split->id();
          $this->configSplitFiles[$filename] = $item;
        }
      }
    }
    ksort($this->configSplitModules);
    ksort($this->configSplitFiles);
  }

  /**
   * Returns the name of the yml file.
   *
   * @param string $name
   *   The name of the config.
   *
   * @return object|bool
   *   Returns the name of the yml file.
   */
  protected function findInConfigSplit(string $name) {
    if (!isset($this->configSplitFiles)) {
      try {
        $this->readConfigSplitFiles();
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      }
    }
    return $this->configSplitFiles[$name . '.yml'] ?? FALSE;
  }

  /**
   * Saves changed config to a configurable directory.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Public function onConfigSave event.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    if ($this->active && $this->enabled()) {
      $name = $event->getConfig()->getName();
      if ($configSplit = $this->findInConfigSplit($name)) {
        $storage = $this->configStorage->createCollection('config_split.' . $configSplit->config_split_id);
      }
      else {
        $storage = $this->configStorage;
      }
      // Read config that will be exported.
      $data = $this->configCache->read($name);
      // Was a module enabled/disabled?
      if ($name === 'core.extension' && !empty($this->configSplitModules)) {
        // Iterate over config split modules.
        foreach ($this->configSplitModules as $splitModule => $value) {
          // Is a "split-enabled" module in the module list?
          if (array_key_exists($splitModule, $data['module'])) {
            // Remove split module from module list.
            unset($data['module'][$splitModule]);
          }
        }
      }
      if ($data === FALSE) {
        $deleteName = $name;
        $name = '.' . $name;
        $data = ['status' => 'deleted'];
      }
      else {
        $deleteName = '.' . $name;
      }
      if ($storage->exists($deleteName)) {
        $storage->delete($deleteName);
      }
      $storage->write($name, $data);
      $this->triggerNeeded = TRUE;
    }
  }

  /**
   * Saves changed config translation to a configurable directory.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   Public function onConfigTranslationSave event.
   */
  public function onConfigTranslationSave(LanguageConfigOverrideCrudEvent $event): void {
    if ($this->active && $this->enabled()) {
      $object = $event->getLanguageConfigOverride();
      $configLanguageStorage = $this->configStorage->createCollection('language.' . $object->getLangcode());
      $configLanguageStorage->write($object->getName(), $object->get());
      $this->triggerNeeded = TRUE;
    }
  }

  /**
   * Turn off this subscriber on importing configuration.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   Public function onConfigImportValidate event.
   *
   * @noinspection PhpUnusedParameterInspection*/
  public function onConfigImportValidate(ConfigImporterEvent $event): void {
    $this->active = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 0];
    $events[ConfigEvents::DELETE][] = ['onConfigSave', 0];
    $events[ConfigEvents::IMPORT_VALIDATE][] = ['onConfigImportValidate', 1024];
    if (class_exists(LanguageConfigOverrideEvents::class)) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE][] = [
        'onConfigTranslationSave',
        0,
      ];
    }
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct(): void {
    if ($this->triggerNeeded) {
      if ($delay = $this->config->get('delay')) {
        if (!$this->config->get('delay_from_first') || !$this->state->get($this->service::STATE_KEY_DUE_TIMESTAMP)) {
          $this->state->set($this->service::STATE_KEY_DUE_TIMESTAMP, $this->time->getRequestTime() + $delay);
        }
      }
      elseif (!$this->service->triggerExport()) {
        // May be paused, so schedule it.
        $this->state->set($this->service::STATE_KEY_DUE_TIMESTAMP, $this->time->getRequestTime());
      }
    }
  }

}
